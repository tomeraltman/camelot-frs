## OS packages installed:
## make python-pip

## Default shell:
SHELL = /bin/bash

## We create this environment variable, so that I can override the default python interpreter on Curie from the make command-line invocation.
## Example: make PYTHON=python2.7 create-package
PYTHON = python3

create-package:
	$(PYTHON) -m pip install --user --upgrade setuptools wheel twine
	$(PYTHON) setup.py sdist bdist_wheel	

clean-package:
	rm -rf dist camelot_frs.egg-info build

upload-package:
	$(PYTHON) -m twine upload --repository pypi dist/*

clean: clean-package

install-package:
	$(PYTHON) -m pip install --user .

install-dev-package:
	$(PYTHON) -m pip install --user -e .

install-dist-package:
	$(PYTHON) -m pip install --user dist/*.whl
